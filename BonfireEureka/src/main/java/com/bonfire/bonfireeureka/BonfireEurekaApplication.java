package com.bonfire.bonfireeureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class BonfireEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(BonfireEurekaApplication.class, args);
    }

}
