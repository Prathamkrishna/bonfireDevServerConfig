package com.bonfire.bonfireapi.models;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class SpotifyGetUserTopArtists {
    private String auth;
    private int top;
}
