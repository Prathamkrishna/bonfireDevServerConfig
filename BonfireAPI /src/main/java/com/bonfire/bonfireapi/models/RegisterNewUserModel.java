package com.bonfire.bonfireapi.models;

import lombok.Data;

@Data
public class RegisterNewUserModel {
    private String authAPItoken;
}