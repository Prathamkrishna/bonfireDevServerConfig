package com.bonfire.bonfireapi.dtos.fetchtopuserartistsdtos;

import lombok.Data;

@Data
public class SpotifyArtistImageDto {
    private String url;
}
