package com.bonfire.bonfireapi.dtos.fetchtopuserartistsdtos;

import lombok.Data;

@Data
public class SpotifyUserTopArtistsDto {
    private SpotifyUserItemsDto[] items;
}

