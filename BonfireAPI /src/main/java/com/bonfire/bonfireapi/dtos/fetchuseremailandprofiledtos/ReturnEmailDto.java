package com.bonfire.bonfireapi.dtos.fetchuseremailandprofiledtos;

import lombok.Data;

@Data
public class ReturnEmailDto {
    private String email = "";
    private Boolean user_exists = true;
}
