package com.bonfire.bonfireapi.dtos.fetchtopuserartistsdtos;

import lombok.Data;

@Data
public class SpotifyUserItemsDto {
    private String name;
    private SpotifyArtistImageDto[] images;
}
