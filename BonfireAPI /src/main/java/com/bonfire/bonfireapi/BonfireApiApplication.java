package com.bonfire.bonfireapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class BonfireApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BonfireApiApplication.class, args);
    }

}
