package com.bonfire.bonfireapi.controllers;

import com.bonfire.bonfireapi.dtos.fetchtopuserartistsdtos.SpotifyUserTopArtistsDto;
import com.bonfire.bonfireapi.models.SpotifyGetUserTopArtists;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController("/api")
public class PlaylistExplorationController {

    private final String spotifyApiRequestController = "http://BONFIRESPOTIFYAUTHSERVICE/";

    private final RestTemplate restTemplate;

    public PlaylistExplorationController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @GetMapping("/")
    public ResponseEntity<SpotifyUserTopArtistsDto> hi(){
        SpotifyGetUserTopArtists userTopArtists = new SpotifyGetUserTopArtists( "BQDuMuJ3hOi0JeLFtqxKeIJ9YhIS2voMlPb60qp9CGzOpX8oRFTTNPiUaV6qzNO2dAmW0WXQH3YEdoq2PBzkyVpmDTlhuQ10c2SwFs9eZIfn2usb8dJoVnRh-D2t38CSkMdE0MCXu_t_0ifpxGyh3qRm_3BWXc4YunVMtJN_WoJAidPJHj_TRR8GdVMXxw", 2);
        return restTemplate.postForEntity(spotifyApiRequestController, userTopArtists, SpotifyUserTopArtistsDto.class);
    }

}
