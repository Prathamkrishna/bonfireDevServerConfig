package com.bonfire.bonfireapi.controllers;

import com.bonfire.bonfireapi.dtos.fetchuseremailandprofiledtos.ReturnEmailDto;
import com.bonfire.bonfireapi.models.RegisterNewUserModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController("/")
public class RegistrationController {

    private final String spotifyApiRequestController = "http://BONFIRESPOTIFYAUTHSERVICE/";

    private final RestTemplate restTemplate;

    public RegistrationController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @PostMapping("/fetchEmail")
    public ResponseEntity<ReturnEmailDto> fetchUserEmail(@RequestBody RegisterNewUserModel registerNewUserModel){
        return restTemplate
                .postForEntity(spotifyApiRequestController + "/getuseremail", registerNewUserModel, ReturnEmailDto.class);
    }
}
